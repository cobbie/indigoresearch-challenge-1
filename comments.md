# Comments

## Design

Some spacing and capitalization were not followed to spec. Since I know you designed your own rand.ai, I'll let it pass. You can try making some fonts smaller to better your font-heirarchy. Try leaving default values as well or placeholders so that your inputs aren't blank when it first opens.

## End notes

Great work implementing your own vanilla server. For this project, defining a `Content-Type` header may be unneccessary, but glad you went the extra mile to even implement a `application/octet-stream` fallback.